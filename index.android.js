import React, { PropTypes ,Component} from 'react';
import {
  Platform,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Text,
  Switch,
  AppRegistry,
} from 'react-native';
import { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps';
import DisplayLatLng from './app/components/Map/DisplayLatLng';
import ViewsAsMarkers from './app/components/Map/ViewsAsMarkers';
const styles = StyleSheet.create({
 container: {
   ...StyleSheet.absoluteFillObject,
   justifyContent: 'flex-end',
   alignItems: 'center',
 },
 map: {
   ...StyleSheet.absoluteFillObject,
 },
});

class HelloWorldApp extends Component {
  render() {
    return (
      <View style ={styles.container}>
        <ViewsAsMarkers provider={PROVIDER_DEFAULT}></ViewsAsMarkers>
     </View>
    );
  }
}
import { NativeModules } from 'react-native';

let { AndroidGeolocation } = NativeModules;
var RNAndroidGeoExample = React.createClass({
  getInitialState: function() {
    AndroidGeolocation.getCurrentLocation(
      (position) => this.setState({position: position}),
      (error) => this.setState({error: error})
    );
    return {
      position: {
        coords: {
          lat: 'Unknown',
          lng: 'Unknown'
        }
      },
      error: ''
    };
  },
  render: function() {
    var content;
    if( this.state.error === '' ) {
      content = (
        <Text style={styles.text}>
          {'You are at: \nLat: ' + this.state.position.coords.latitude + '\nLng: ' + this.state.position.coords.longitude}
        </Text>
      );
    } else {
      content = (
        <Text style={styles.text}>
          {this.state.error}
        </Text>
      );
    }
    return (
      <View style={styles.container}>
        {content}
      </View>
    );
  }
});
class NativeModuleDev extends Component {
  render() {
    return (
      <View style ={styles.container}>
     </View>
    );
  }
}

AppRegistry.registerComponent('RNMaps', () => HelloWorldApp);
