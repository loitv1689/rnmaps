import React, { PropTypes } from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import Location from './Location';

const LocationContainer = ({ locationReady }) => {
  return (
    <Location
      locationReady={locationReady}
    />
  );
};

LocationContainer.propTypes = {
  locationReady: PropTypes.bool,
};

export default createContainer(() => {
  const handle = Meteor.subscribe('details-list');
  return {
    locationReady: handle.ready(),
  };
}, LocationContainer);
