import React, { PropTypes } from 'react';
import { MeteorListView } from 'react-native-meteor';
import Loading from '../../components/Loading';
import {
  Platform,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Text,
  Switch,
} from 'react-native';
import { PROVIDER_GOOGLE, PROVIDER_DEFAULT } from 'react-native-maps';
import DisplayLatLng from '../../components/Map/DisplayLatLng';
import ViewsAsMarkers from '../../components/Map/ViewsAsMarkers';
import EventListener from '../../components/Map/EventListener';
import MarkerTypes from '../../components/Map/MarkerTypes';
import DraggableMarkers from '../../components/Map/DraggableMarkers';
import PolygonCreator from '../../components/Map/PolygonCreator';
import PolylineCreator from '../../components/Map/PolylineCreator';
import AnimatedViews from '../../components/Map/AnimatedViews';
import AnimatedMarkers from '../../components/Map/AnimatedMarkers';
import Callouts from '../../components/Map/Callouts';
import Overlays from '../../components/Map/Overlays';
import DefaultMarkers from '../../components/Map/DefaultMarkers';
import CustomMarkers from '../../components/Map/CustomMarkers';
import CachedMap from '../../components/Map/CachedMap';
import LoadingMap from '../../components/Map/LoadingMap';
import TakeSnapshot from '../../components/Map/TakeSnapshot';
import FitToSuppliedMarkers from '../../components/Map/FitToSuppliedMarkers';
import FitToCoordinates from '../../components/Map/FitToCoordinates';
import LiteMapView from '../../components/Map/LiteMapView';
import CustomTiles from '../../components/Map/CustomTiles';
import StaticMap from '../../components/Map/StaticMap';

const IOS = Platform.OS === 'ios';
const ANDROID = Platform.OS === 'android';

const styles = StyleSheet.create({
 container: {
   ...StyleSheet.absoluteFillObject,
   justifyContent: 'flex-end',
   alignItems: 'center',
 },
 map: {
   ...StyleSheet.absoluteFillObject,
 },
});
const Location = ({ locationReady }) => {

  return (
     <View style ={styles.container}>
        <ViewsAsMarkers provider={PROVIDER_DEFAULT}></ViewsAsMarkers>
     </View>
  );
};

Location.propTypes = {
  locationReady: PropTypes.bool,
};

export default Location;
