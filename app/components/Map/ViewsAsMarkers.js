import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import MapView from 'react-native-maps';
import PriceMarker from './PriceMarker';
import { NativeModules } from 'react-native';
const { width, height } = Dimensions.get('window');
const { AndroidGeolocation } = NativeModules;  
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
class ViewsAsMarkers extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      coordinate: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
      },
      coordinate1: {
        latitude: 37.75231,
        longitude: -122.401232,
      },
      coordinate2: {
        latitude: 37.78231,
        longitude: -122.401232,
      },
      markers: [],
      circles:[],
      amount: 99,
    };
   
  }
  getListPositions(){
    //fetch position here
    return {

    };
  }
  processPosition(position){
     console.log(position);
          // this.state.region.longitude = position.coords.longitude;
      isShowCircle = true;
      let markers = [];
      const { region } = this.state;
      jobs = fetch('http://geolocation.vnjware.com/get_markers')
      .then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson);
        let jobs = responseJson.markers;
        console.log(jobs);
        let length = jobs.length;
        for(var i =0; i < length;i++){
          markers[i] = {
            coordinate: {
              latitude: jobs[i].lat,
              longitude: jobs[i].lng,
            },
            title: jobs[i].title,
            key: i
          }
        }
        this.setState({
            markers: markers
        });
      })
      .catch((error) => {
        console.error(error);
      });

      circles = [];
      circles[1] = {
        center: {
          latitude: position.coords.latitude ,
          longitude: position.coords.longitude,
        },
        radius: 3000,
        key:1
      };
      this.setState({
            circles: circles
      });
      this.setState({
            circles: circles
      });
      this.setState({
              'region.latitude': position.coords.latitude
        });                
      this.setState({
              'region.longitude': position.coords.longitude
        });
      this.map.animateToRegion({
        ...this.state.region,
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      });
      console.log(this.state.region);
  }
  componentGetPosition() {
    if (Platform.OS !== 'ios'){
      AndroidGeolocation.getCurrentLocation(
        (position) =>{ 
          this.processPosition(position) ;
      },
        (error) => alert(JSON.stringify(error))
      );  
    }else{
        navigator.geolocation.getCurrentPosition(
        (position) => {
          this.processPosition(position) ;
        },
        (error) => alert(JSON.stringify(error)),
        {   
          enableHighAccuracy: true,
          timeout: 5000,
          maximumAge: 0
        }
      );  
    }
    
    
  }
  randomRegion() {
    const { region } = this.state;
    return {
      ...this.state.region,
      latitude: region.latitude + ((Math.random() - 1.5) * (region.latitudeDelta / 2)),
      longitude: region.longitude + ((Math.random() - 1.5) * (region.longitudeDelta / 2)),
    };
  }

  increment() {
    this.setState({ amount: this.state.amount + 1 });
    this.map.animateToRegion(this.randomRegion());
     console.log(this.state.region.longitude);
  }

  jumpRandom() {
    this.setState({ region: this.randomRegion() });
  }

  animateRandom() {
    this.map.animateToRegion(this.randomRegion());
  }
  decrement() {
    this.setState({ amount: this.state.amount - 1 });
  }
  onRegionChange(region) {
    this.setState({ region });
  }
  renderCircle(){
    const { circle } = this.state;
    if(isShowCircle){
      return (
        <View>
            <MapView.Circle
            center={circle.center}
            radius={circle.radius}
            fillColor="rgba(200, 0, 0, 0.5)"
            strokeColor="rgba(0,0,0,0.5)"
          />
        </View>
      );
    }
  }
  refreshMarker(){
      this.componentGetPosition();
  }
  render() {
    
    return (
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          ref={ref => { this.map = ref; }}
        >
        {
          this.state.markers.map(marker => (
            <MapView.Marker coordinate={marker.coordinate} key={marker.key}>
              <PriceMarker title={marker.title} />

            </MapView.Marker>
          ))
        }
        {
          this.state.circles.map(circle => (
            <MapView.Circle 
            center={circle.center}
            radius={circle.radius}
            fillColor="rgba(230, 242, 255,0.6)"
            strokeColor="rgba(230, 242, 255,0.6)"
            key={circle.key}
            />
          )) 
        }
        </MapView>
          <TouchableOpacity
            onPress={() => this.refreshMarker()}
            style={[styles.bubble, styles.button]}
          >
            <Text>Refresh</Text>
          </TouchableOpacity>
      </View>
    );
  }
}

ViewsAsMarkers.propTypes = {
  provider: MapView.ProviderPropType,
};


const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'red'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

module.exports = ViewsAsMarkers;
